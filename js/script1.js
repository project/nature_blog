jQuery(document).ready(function () {
    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() > 100) {
          jQuery('.scrolltop').fadeIn('slow');
        } else {
          jQuery('.scrolltop').fadeOut('slow');
        }
      });
      jQuery('.scrolltop').click(function () {
        jQuery('html, body').animate( { scrollTop: 0 }, 'slow');
      });
    });
var sidebar=document.getElementById('sidebar');
       var toggleButton=document.getElementById('toggle-icon');

       toggleButton.addEventListener('click',function(){
           if (sidebar.classList.contains('show')){
               sidebar.classList.remove('show');
               sidebar.classList.add('hide');
           }else{
            sidebar.classList.add('show');
            sidebar.classList.remove('hide');
           }

       })
